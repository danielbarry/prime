// -- Includes --

#include <climits>
#include <cmath>
#include <iostream>
#include <fstream>

/**
 * A simple class responsible for generating primes.
 *
 * Written by B[]Array
 **/

// -- Static Methods --
static void printPrime(unsigned long long num);

/**
 * main()
 *
 * This is where our program runs it's prime loop.
 *
 * @return The error code when the program exits
 **/
int main(int argCount, char** args){
  // Print title
  std::cout << "  ______________________\n"
            << " /                      \\\n"
            << "< Simple Prime Generator >\n"
            << " \\______________________/\n"
            << "\n";

  // Initialize Variables
  // NOTE: By default the starting prime is three once we have dealt with two
  unsigned long long i = 3;
  // NOTE: By default we generate 100 primes
  unsigned long long n = 100;

  // Itterate through arguments
  for(int x = 0; x < argCount; x++){
    // Whether a long name has been presented
    bool exit = false;

    if(args[x][0] == '-'){
      // Double argumens
      if(args[x][1] == '-'){
        exit = true;
        // Store the first letter of the word where it is expected
        args[x][1] = args[x][2];
      }

      // Single arguments
      for(int a = 1; args[x][a] > ' ' && a + 1 < argCount; a++){
        // Search for single arguments
        switch(args[x][a]){
          case 'a' :
            std::cout << "  ABOUT PRIME GENERATOR\n"
                      << "\n"
                      << "    Written by B[]Array 2014\n"
                      << "\n"
                      << "    DESIGN\n"
                      << "\n"
                      << "      Originally designed for Linux, this program\n"
                      << "      should be cross platform and compilable with\n"
                      << "      most c++ compilers.\n"
                      << "\n"
                      << "    USES\n"
                      << "\n"
                      << "      There are many ideas for how this program may\n"
                      << "      be used, the following are the uses I thought\n"
                      << "      of:\n"
                      << "\n"
                      << "        - Determine processor speed\n"
                      << "        - Generate large primes\n"
                      << "        - Generate lots of primes\n"
                      << "        - Turn a computer into a room heater\n"
                      << "\n"
                      << "        There may be others.\n"
                      << "\n";
            // NOTE: Return here so that user can view output.
            return 0;
          case 'd' :
            std::cout << "Debug mode is for programs that go wrong. Therefore\n"
                      << "in this program, debug mode is not implemented.\n";
            // NOTE: Return here so that user can view output.
            return 0;
          case 'e' :
            // Set x to read the next value
            x++;
            // Start with n from scratch
            n = 0;
            // Itterate over characters
            for(int z = 0; n < ULLONG_MAX; z++){
              // Check to see if we have reached the end
              if(args[x][z] <= ' ')
                break;
              // Times what was in there last by 10
              n *= 10;
              // Add new unit value
              n += args[x][z] - '0';
            }
            // Let user know that we have changed the value as per request
            std::cout << "Processing " << n << " primes.\n";
            break;
          case 'h' :
            std::cout << "  PRIME GENERATOR HELP\n"
                      << "\n"
                      << "  prime [OPTIONS...]\n"
                      << "\n"
                      << "    OPTIONS\n"
                      << "\n"
                      << "      -a    About the program\n"
                      << "        --about\n"
                      << "      -d    Debug mode\n"
                      << "        --debug\n"
                      << "      -e    Define the maximum prime to find\n"
                      << "        --end\n"
                      << "      -h    Displays this help text\n"
                      << "        --help\n"
                      << "      -v    Verbose mode\n"
                      << "        --verbose\n"
                      << "\n"
                      << "    EXAMPLES\n"
                      << "\n"
                      << "      Outputs primes to the file `output.txt`\n"
                      << "        prime output.txt\n"
                      << "\n"
                      << "      Displays this help text\n"
                      << "        prime -h\n"
                      << "\n";
            // NOTE: Return here so that user can view output.
            return 0;
          case 'v' :
            std::cout << "What else do you want to know? Primes are already\n"
                      << "output to a desired location!\n";
            // NOTE: Return here so that user can view output.
            return 0;
          default :
            std::cout << '`' << args[x][a] << '`' << " is not a valid parameter.\n";
            return -1;
        }

        // Check whether we need to exit this loop
        if(exit)
          break;
      }
    }
  }

  // Handle first case
  // NOTE: After this they are all odd
  if(n >= 1)
    printPrime(2);

  // Setup values for main spin
  bool pass = true;
  unsigned long long x = 0;
  unsigned long long max = 0;
  // NOTE: Already processed one number as it's the only prime that's even
  unsigned long long c = 2;

  // Find n primes
  for(; i <= ULLONG_MAX && c <= n; i += 2){
    // Reset Pass
    pass = true;
    // Set maximum number to search
    // NOTE: Added one to prevent bad rounding
    max = sqrt(i) + 1;

    // See if number is divisable by two
    // NOTE: No even numbers need checking after this
    if(i % 2 == 0)
      pass = false;

    // Start searching from 3
    x = 3;

    // Make sure we haven't failed before we begin
    if(pass){
      // Spin until either fail or pass all
      for(; x < max; x += 2){
        // Check values
        if(i % x == 0){
          pass = false;
          break;
        }
      }
    }

    // Check whether it passed the test
    if(pass){
      printPrime(i);
      // Increment our count of primes
      c++;
    }
  }

  // Exit
  std::cout << "\n>> Complete\n";
  return 0;
}

/**
 * printPrime()
 *
 * This method prints a prime to the screen
 *
 * @param num The number to be printed
 **/
void printPrime(unsigned long long num){ std::cout << num << '\t'; }
